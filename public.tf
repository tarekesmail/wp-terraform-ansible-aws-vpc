/*
  Web Servers
*/
resource "aws_security_group" "web" {
    name = "vpc_web"
    description = "Allow incoming HTTP connections."

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        protocol  = -1
        self      = true
        from_port = 0
        to_port   = 0
    }
    egress {
       from_port   = 0
       to_port     = 0
       protocol    = "-1"
       cidr_blocks = ["0.0.0.0/0"]
    }
    egress { # SQL Server
        from_port = 1433
        to_port = 1433
        protocol = "tcp"
        cidr_blocks = ["${var.private_subnet_cidr}"]
    }
    egress { # MySQL
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = ["${var.private_subnet_cidr}"]
    }

    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "WebServer"
    }
}

resource "aws_instance" "web-1" {
    ami = "ami-785db401"
    availability_zone = "eu-west-1a"
    instance_type = "m1.small"
    key_name = "admin_key"
    vpc_security_group_ids = ["${aws_security_group.web.id}"]
    subnet_id = "${aws_subnet.eu-west-1a-public.id}"
    associate_public_ip_address = true
    source_dest_check = false
    depends_on = ["aws_instance.nat"]

    tags {
        Name = "Web Server"
    }
provisioner "local-exec" {
    command = "sleep 20; cat ./keys/admin_key > ~/.ssh/admin_key.pem; chmod 400 ~/.ssh/admin_key.pem"
  }

provisioner "local-exec" {
    command = "sleep 5; echo -e 'Host 10.*.*.*\n  ProxyCommand ssh -W %h:%p ${aws_eip.nat.public_ip}\n  StrictHostKeyChecking no\n  IdentityFile ~/.ssh/admin_key.pem' > ~/.ssh/config"
  }

provisioner "local-exec" {
    command = "sleep 5; echo -e 'Host ${aws_eip.nat.public_ip}\n  Hostname ${aws_eip.nat.public_ip}\n  User ec2-user\n  StrictHostKeyChecking no\n  IdentityFile ~/.ssh/admin_key.pem\n  ControlMaster auto\n  ControlPath ~/.ssh/ansible-%r@%h:%p\n  ControlPersist 5m\n' >> ~/.ssh/config"
  }

provisioner "local-exec" {
    command = "echo [wordpress] > ansible/hosts;echo ${aws_instance.web-1.private_ip} >> ansible/hosts"
  }
provisioner "local-exec" {
    command = "sleep 200; ssh -o StrictHostKeyChecking=no ubuntu@${aws_instance.web-1.private_ip} sudo apt-get update;"

  }
provisioner "local-exec" {
    command = "sleep 5; ssh -o StrictHostKeyChecking=no ubuntu@${aws_instance.web-1.private_ip} sudo apt-get install python -y;"
  }

}

resource "aws_eip" "web-1" {
    instance = "${aws_instance.web-1.id}"
    vpc = true
}


output "WebServer_Private_IP" {
  value = "${aws_instance.web-1.private_ip}"
}

