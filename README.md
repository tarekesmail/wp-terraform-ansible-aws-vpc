Terraform AWS Ansible WordPress Provisioner
---------------------

The purpose of Terraform AWS Modules are to create a fully operational AWS VPC infrastructure(subnets,routeing tables,igw etc), it will also create everything that need to be for creating EC2 (security key, security group, subnet group).



### Terraform AWS Modules Tasks:

- Create 1 x VPC with 4 x VPC subnets(2 x public and 2 x private) in differrent AZ zones inside the AWS region
- Create the AWS key pair with the provided public key using admin_key public rsa key 
- Create 1 x security group for each(SSH,Webservers,MySQL DB Connection)
- Provision 2 x EC2 instances(Ubuntu 16.04 LTS) in 2 different private AZ
- Make the DB Server with only private IP 
- The Web Server With Private and Public IP
- Updating the apt-get repo inside the provisioner for (WebServer and Database Server)
- Installing python using apt-get inside the provisioner for (WebServer and Database Server)
- Generating the SSH config file inside ~/.ssh/config to allow traffic through bastion server
- Adding the SSH Key in ~/.ssh/admin_key.pem

### Terraform Important Notes:

- This Infrastructure will created under region (eu-west-1)
- You can change the region ID and the availability zone in tf files by run 
```shell
sed -i s/eu-west-1/YOUR_ZONE/ *.tf 
```
- This Infrastructure built for Ubuntu 16.04 AMI it's installing the packages using apt-get package manager, You can list the AMI IDs for the ubuntu depending on your Region by visiting the following URL
 [https://cloud-images.ubuntu.com/locator/ec2/](https://cloud-images.ubuntu.com/locator/ec2/)
- For CentOS AMI you can visit the following URL to list them depends on your Region and scroll down to section "Official CentOS Linux : Public Images"
 [https://wiki.centos.org/Cloud/AWS](https://wiki.centos.org/Cloud/AWS)
- The VPC using NAT Image by AWS called amzn-ami-vpc-nat-pv-2014.09.1.x86_64-ebs this image prepared for do NAT by AWS, Please search on it in creating instances window and grab the ID of this AMI to added it to vpc.tf file
- We highly recommend to run this code under CentOS 7 OS 

### Installation Steps

```shell
git clone https://gitlab.com/tarekesmail/wp-terraform-ansible-aws-vpc.git
yum install unzip -y
wget https://releases.hashicorp.com/terraform/0.11.10/terraform_0.11.10_linux_amd64.zip
unzip terraform_0.11.10_linux_amd64.zip
sudo mv terraform /usr/local/bin/
yum install ansible -y 
```

### Tools Used:

```shell
ansible --version
ansible 2.7.4
  config file = ansible/ansible.cfg

terraform version
Terraform v0.11.10
```


Before using the terraform, we need to set `aws_access_key` and `aws_secret_key` in file terraform.tfvars:

```
aws_access_key="yyyyyyyyyyyyyyy"
aws_secret_key="zzzzzzzzzzzzzzzzzzzzzzz"
```

### Ansible Modules Tasks:

- Installing PHP Modules and Web Server in the WebServer through the bastion host which defined in ~/.ssh/config
- Installing Database and Create the Database for wordpress in the Database Server through the bastion host which defined in ~/.ssh/config
- Set the wordpress config for database server and database information using the database server private IP
- Print output of WebServer public ip to use it to completing the wordpress installation 
- After the installation complete and print the web server ip address please visit it using browser and complete the wordpress installation.

To initialize terraform modules you must run
```
terraform init
```

To build the infrastructure and mentioned above you must run (use -auto-approve) to accept yes directly to terraform question
```
terraform apply -auto-approve
```

To completely destroy infrastructure (use -auto-approve) to accept yes directly to terraform question
```
terraform destroy -auto-approve
```



All informations about VPC, Webserver, Database are defined in their respective modules of vpc.tf, public.tf, private.tf. 
