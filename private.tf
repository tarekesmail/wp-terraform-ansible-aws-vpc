/*
  Database Servers
*/
resource "aws_security_group" "db" {
    name = "vpc_db"
    description = "Allow incoming database connections."

    ingress { # SQL Server
        from_port = 1433
        to_port = 1433
        protocol = "tcp"
        security_groups = ["${aws_security_group.web.id}"]
    }
    ingress { # MySQL
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        security_groups = ["${aws_security_group.web.id}"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }

    egress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        protocol  = -1
        self      = true
        from_port = 0
        to_port   = 0
    }
    egress {
       from_port   = 0
       to_port     = 0
       protocol    = "-1"
       cidr_blocks = ["0.0.0.0/0"]
    }
    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "DatabaseServer"
    }
}

resource "aws_instance" "db-1" {
    ami = "ami-785db401"
    availability_zone = "eu-west-1a"
    instance_type = "m1.small"
    key_name = "admin_key"
    vpc_security_group_ids = ["${aws_security_group.db.id}"]
    subnet_id = "${aws_subnet.eu-west-1a-private.id}"
    source_dest_check = false
    depends_on = ["aws_instance.web-1"]

    tags {
        Name = "Database Server"
    }
provisioner "local-exec" {
    command = "echo [database] >> ansible/hosts;echo ${aws_instance.db-1.private_ip} >> ansible/hosts"
  }
provisioner "local-exec" {
    command = "sleep 200; ssh -o StrictHostKeyChecking=no ubuntu@${aws_instance.db-1.private_ip} sudo apt-get update;"

 }
provisioner "local-exec" {
    command = "sleep 5; ssh -o StrictHostKeyChecking=no ubuntu@${aws_instance.db-1.private_ip} sudo apt-get install python -y;"
 }
provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i ansible/hosts ansible/playbook.yml --extra-vars 'WEB=${aws_instance.web-1.private_ip} DB=${aws_instance.db-1.private_ip}'; ./output.sh ${aws_eip.web-1.public_ip}"
  }

}

output "Database_Private_IP" {
  value = "${aws_instance.db-1.private_ip}"
}
